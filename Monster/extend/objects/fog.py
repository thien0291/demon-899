from baseobject import GameObject
import sys

class FogFactory():
    '''Factory pattern: use for creating model in this module.'''
    @staticmethod
    def createFog(fogType, *args, **kwargs):
        fog = getattr(sys.modules[__name__], 'FogBase')(*args, **kwargs)
        return fog
    
class FogBase(GameObject):
    def __init__(self, *args, **kwargs):
        GameObject.__init__(self, *args, **kwargs)        
        
    def doNothing(self, *args, **kwargs):
        pass
    
    def getMode(self):
        return self.nodePath.getMode()

