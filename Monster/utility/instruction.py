'''All game useful function here'''
from direct.gui.OnscreenText import OnscreenText
from panda3d.core import WindowProperties, TextNode

def getScreenRatio():
    props = WindowProperties(base.win.getProperties())
    return float(props.getXSize()) / float(props.getYSize())

def addText(pos, msg, changeable=False, alignLeft=True, scale=0.05):
    x = -getScreenRatio() + 0.03
    if alignLeft:
        align = TextNode.ALeft
    else:
        align = TextNode.ARight
        x *= -1.0
    return OnscreenText(text=msg, style=1, fg=(1, 1, 1, 1),
                        pos=(x, pos), align=align, scale=scale,
                        mayChange=changeable)